//UI Dependencies
import { Component, OnInit } from "@angular/core";
import {Button} from "ui/button";
import {TextField} from "ui/text-field";
import { ListPicker } from "ui/list-picker";
import { AgentConfig } from "../models/agent";

//Location depencencies
// import * as geolocation from "nativescript-geolocation";
import { isEnabled, enableLocationRequest, getCurrentLocation, watchLocation, distance, clearWatch } from "nativescript-geolocation";
import { Accuracy } from "ui/enums"; // used to describe at what accuracy the location should be get

import firebase = require("nativescript-plugin-firebase");
//@todo change how to add dependencies
import observable = require("data/observable");
import view = require("ui/core/view");
import label = require("ui/label");

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})

export class HomeComponent implements OnInit {
    isEnabledLocation:boolean = false;
    isListeningLocation:boolean = false;

    agentConfig: AgentConfig = new AgentConfig();

    agentName:string;
    agentID:string;
    agentPhoneNumber:string;
    textButton:string;
    agentType:number = 0;

    watchId:number = -1;

    readonly labelIniciar = "Iniciar";
    readonly labelDesligar = "Desligar";
    readonly agentTypes = ['BHTRANS', 'Defesa Civil', 'Guarda Municipal', 'SLU', 'SAMU', 'SUFIS']

    constructor() {
        this.agentConfig.init();
    }

    ngOnInit(): void {
        console.log("agentConfig: "+this.agentConfig);
        this.textButton = this.labelIniciar;
        // Init your component properties here.
        console.log("ngOnInit!");
        isEnabled().then(isEnabled => {
            this.isEnabledLocation = isEnabled;
            if (!isEnabled) {
                enableLocationRequest().then(function () {
                }, function (e) {
                    console.log("Error: " + (e.message || e));
                });
            }
        }, function (e) {
            console.log("Error: " + (e.message || e));
        });

        if(this.agentConfig){
            this.agentName = this.agentConfig.name;
            this.agentID = this.agentConfig.ID;
            this.agentPhoneNumber = this.agentConfig.phoneNumber;
            this.agentType = this.agentConfig.type;
        }
    }

    tapStart(args: observable.EventData) {

        if(this.isListeningLocation){
            clearWatch(this.watchId);
            this.watchId = -1;
            this.textButton = this.labelIniciar;
            this.isListeningLocation = false;
            console.log("Não escutando a localização");
        } else if(this.isEnabledLocation){
            if(this.watchId == -1){
                console.log("Escutando localização");
                this.watchId = watchLocation(
                    loc => {
                        if (loc) {
                            console.log("Received location: " + loc);
                            this.saveLocation(loc.latitude, loc.longitude);
                        }
                    }, 
                    e => {
                        console.log("Error: " + e.message);
                    }, 
                    {desiredAccuracy: 3, updateDistance: 1, minimumUpdateTime : 1000 * 1}
                );
                
                this.isListeningLocation = this.watchId != -1;
            }
            this.textButton = this.labelDesligar;
            alert("Acompanhamento da localização iniciado!");
        }
    }

    selectedIndexChanged(args: observable.EventData) {
        let picker = <ListPicker>args.object;
        console.log("picker selection: " + picker.selectedIndex);
        this.agentConfig.type = picker.selectedIndex;
        this.updateConfig();
    }

    saveLocation(lat, long):void{
        console.log(`Received lat: ${lat}, long: ${lat}`);
        firebase.setValue(`/agentes/${this.agentConfig.ID}`, {
            matricula: this.agentConfig.ID,
            nome: this.agentConfig.name,
            telefone: this.agentConfig.phoneNumber,
            tipo: this.agentConfig.type,
            lat: lat,
            lon: long,
        });
    }

    onReturnPress(args: observable.EventData) {
        const textField = <TextField>args.object;
        if(textField.text){
            this.updateConfig(textField);
        }
        setTimeout(() => {
            textField.dismissSoftInput(); // Hides the soft input method, ususally a soft keyboard.
        }, 100);
    }
    
    onTextChange(args: observable.EventData){
        const textField = <TextField>args.object;
        this.updateConfig(textField);
    }

    updateConfig(textField: TextField = null){
        if(textField){
            switch (textField.id){
                case "agentName" :{
                    this.agentConfig.name = textField.text;
                    break;
                }
                case "agentID" :{
                    this.agentConfig.ID = textField.text;
                    break;
                }
                case "agentPhoneNumber" :{
                    this.agentConfig.phoneNumber = textField.text;
                    break;
                }
            }
        }

        if(this.agentConfig){
            this.agentConfig.save();
            console.log("Salvando configuração: "+JSON.stringify(this.agentConfig));
        }
    }
}