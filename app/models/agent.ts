import appSettings = require("application-settings");

export class AgentConfig{
    name:string;
    ID:string;
    phoneNumber:string;
    type:number;

    init(){
        this.name = appSettings.getString("name");
        this.ID = appSettings.getString("ID");
        this.phoneNumber = appSettings.getString("phoneNumber");
        this.type = appSettings.getNumber("type", 0);
    }

    save(){
        appSettings.setString("name", this.name);
        appSettings.setString("ID", this.ID);
        appSettings.setString("phoneNumber", this.phoneNumber);
        appSettings.setNumber("type", this.type);
    }
}